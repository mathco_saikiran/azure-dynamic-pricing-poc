import sys
import logging
from datetime import datetime
import time
import os
import pandas as pd
import random
import json

from azure.eventhub import EventHubClient, Sender, EventData

logger = logging.getLogger("azure")

ADDRESS = "sb://dynamicpricing.servicebus.windows.net/customer_streaming_data"


USER = "RootManageSharedAccessKey"
KEY = "VOkO7h1Mpssp/eJJLd3ZRTr4wMSGd/dv+KYDzDBbviI="

try:
    if not ADDRESS:
        raise ValueError("No EventHubs URL supplied.")

    ##Create Event Hubs client
    client = EventHubClient(ADDRESS, debug=False, username=USER, password=KEY)
    sender = client.add_sender(partition="0")
    client.run()
    try:
        start_time = time.time()
        while 1==1:
        #for i in range():
            customer_data = pd.read_excel('Excel_files/Customer_1000_streaming_data.xlsx')
            customer_data = customer_data.sample().reset_index()

            location_data = pd.read_excel('Excel_files/Latitude_and_Logitude_Region_wise.xlsx')
            location_data_pickup = location_data.sample()
            location_data_drop = location_data.sample()

            location_data_pickup = location_data_pickup[['Latitude','Longitude']].reset_index()
            location_data_pickup.rename(columns = {'Latitude': 'Pickup_Latitude', 'Longitude': 'Pickup_Longitude'}, inplace=True)

            location_data_drop = location_data_drop[['Latitude','Longitude']].reset_index()
            location_data_drop.rename(columns = {'Latitude': 'Drop_Off_Latitude', 'Longitude': 'Drop_Off_Longitude'}, inplace=True)

            random_data = pd.DataFrame()
            random_data = pd.concat([customer_data, location_data_pickup, location_data_drop], axis=1)
            
            eventjson= json.dumps({'customer_id': str(random_data['customer_id'].values[0]), 'customer_name': str(random_data['customer_name'].values[0]), 'customer_phone_number': str(random_data['customer_phone_number'].values[0]), 'Pickup_Latitude': str(random_data['Pickup_Latitude'].values[0]), 'Pickup_Longitude': str(random_data['Pickup_Longitude'].values[0]), 'Drop_Off_Latitude': str(random_data['Drop_Off_Latitude'].values[0]), 'Drop_Off_Longitude': str(random_data['Drop_Off_Longitude'].values[0]), 'time': str(datetime.now())})
            print(f"customer_id: {str(random_data['customer_id'].values[0])}, customer_name: {str(random_data['customer_name'].values[0])}, customer_phone_number: {str(random_data['customer_phone_number'].values[0])}, Pickup_Latitude: {str(random_data['Pickup_Latitude'].values[0])}, Pickup_Longitude: {str(random_data['Pickup_Longitude'].values[0])}, Drop_Off_Latitude: {str(random_data['Drop_Off_Latitude'].values[0])}, Drop_Off_Longitude: {str(random_data['Drop_Off_Longitude'].values[0])}, time: {str(datetime.now())}")            
            sender.send(EventData(eventjson))
    except:   
        raise
    finally:
        end_time = time.time()
        client.stop()
        run_time = end_time - start_time
        logger.info("Runtime: {} seconds".format(run_time))

except KeyboardInterrupt:
    pass