import sys
import logging
from datetime import datetime
import time
import os
import pandas as pd
import random
import json

from azure.eventhub import EventHubClient, Sender, EventData

logger = logging.getLogger("azure")

ADDRESS = "sb://dynamicpricing.servicebus.windows.net/customer_streaming_data"

USER = "RootManageSharedAccessKey"
KEY = "VOkO7h1Mpssp/eJJLd3ZRTr4wMSGd/dv+KYDzDBbviI="

try:
    if not ADDRESS:
        raise ValueError("No EventHubs URL supplied.")

    client = EventHubClient(ADDRESS, debug=False, username=USER, password=KEY)
    sender = client.add_sender(partition="4")
    client.run()
    try:
        start_time = time.time()
        while 1==1:
        #for i in range():
            driver_data = pd.read_excel('Excel_files/Driver_500_streaming_data.xlsx')
            driver_data = driver_data.sample().reset_index()
    
            location_data = pd.read_excel('Excel_files/Latitude_and_Logitude_Region_wise.xlsx')
            location_data = location_data.sample()
            location_data = location_data[['Latitude','Longitude']].reset_index()
            #print(location_data)
            status_data = ['Free','Busy']
            status_data = random.choice(status_data)
            print(status_data)
            driver_data['status'] = status_data
            random_data = pd.DataFrame()
            random_data = pd.concat([driver_data, location_data], axis=1)
            #print(random_data)
            #print(random_data['customer_id'].values[0])
            #print(random_data.iloc[0,'customer_name'])
            #print(random_data.iloc[0,'customer_phone_number'])
            #print(random_data.iloc[0,'Latitude'])
            #print(andom_data.iloc[0,'Longitude'])
            
            #print(f"Device: {devicenumber}, Reading: {readingnumber},time: {str(datetime.now())}")
            eventjson= json.dumps({'driver_id': str(random_data['driver_id'].values[0]), 'driver_name': str(random_data['driver_name'].values[0]), 'driver_phone_number': str(random_data['driver_phone_number'].values[0]), 'car_model': str(random_data['car_model'].values[0]), 'car_number': str(random_data['car_number'].values[0]), 'Latitude': str(random_data['Latitude'].values[0]), 'Longitude': str(random_data['Longitude'].values[0]), 'status': str(random_data['status'].values[0]), 'time': str(datetime.now())})
            print(f"driver_id: {str(random_data['driver_id'].values[0])}, driver_name: {str(random_data['driver_name'].values[0])}, driver_phone_number: {str(random_data['driver_phone_number'].values[0])}, car_model: {str(random_data['car_model'].values[0])}, car_number: {str(random_data['car_number'].values[0])}, Latitude: {str(random_data['Latitude'].values[0])}, Longitude: {str(random_data['Longitude'].values[0])}, status: {str(random_data['status'].values[0])} time: {str(datetime.now())}")            
            sender.send(EventData(eventjson))
    except:   
        raise
    finally:
        end_time = time.time()
        client.stop()
        run_time = end_time - start_time
        logger.info("Runtime: {} seconds".format(run_time))

except KeyboardInterrupt:
    pass