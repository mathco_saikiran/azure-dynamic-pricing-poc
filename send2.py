import sys
import logging
from datetime import datetime
import time
import os
import random
import json

from azure.eventhub import EventHubClient, Sender, EventData

logger = logging.getLogger("azure")

# Address can be in either of these formats:
# "amqps://<URL-encoded-SAS-policy>:<URL-encoded-SAS-key>@<mynamespace>.servicebus.windows.net/myeventhub"
# "amqps://<mynamespace>.servicebus.windows.net/myeventhub"
# For example:
ADDRESS = "sb://dynamicpricing.servicebus.windows.net/dynamicpricing-eventhub1"

# SAS policy and key are not required if they are encoded in the URL
USER = "RootManageSharedAccessKey"
KEY = "VOkO7h1Mpssp/eJJLd3ZRTr4wMSGd/dv+KYDzDBbviI="

try:
    if not ADDRESS:
        raise ValueError("No EventHubs URL supplied.")

    ##Create Event Hubs client
    client = EventHubClient(ADDRESS, debug=False, username=USER, password=KEY)
    sender = client.add_sender(partition="0")
    client.run()
    try:
        start_time = time.time()
        #while 1==1:
        for i in range(100):
            devicenumber = random.randint(1,1000)
            readingnumber = random.random()
            print("***** SEND 2****")
            print(f"Device: {devicenumber}, Reading: {readingnumber},time: {str(datetime.now())}")
            eventjson= json.dumps({'device': devicenumber, 'reading': readingnumber, 'time': str(datetime.now())})
            sender.send(EventData(str(eventjson)))
    except:
        raise
    finally:
        end_time = time.time()
        client.stop()
        run_time = end_time - start_time
        logger.info("Runtime: {} seconds".format(run_time))

except KeyboardInterrupt:
    pass