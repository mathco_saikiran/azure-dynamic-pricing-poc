import os
import sys
import logging
import time
import json
from azure.eventhub import EventHubClient, Receiver, Offset

logger = logging.getLogger("azure")

# Address can be in either of these formats:
# "amqps://<URL-encoded-SAS-policy>:<URL-encoded-SAS-key>@<mynamespace>.servicebus.windows.net/myeventhub"
# "amqps://<mynamespace>.servicebus.windows.net/myeventhub"
# For example:
ADDRESS = "sb://dynamicpricing.servicebus.windows.net/dynamicpricing-eventhub1"

# SAS policy and key are not required if they are encoded in the URL
USER = "RootManageSharedAccessKey"
KEY = "VOkO7h1Mpssp/eJJLd3ZRTr4wMSGd/dv+KYDzDBbviI="

CONSUMER_GROUP = "$default"
OFFSET = Offset("-1")
PARTITION = "0"

##COUNTER
total = 0
last_sn = -1
last_offset = 1

##SET CLIENT
client = EventHubClient(ADDRESS, debug=False, username=USER, password=KEY)

##PROCESS QUEUED DATA
try:
    receiver = client.add_receiver(CONSUMER_GROUP, PARTITION, prefetch=5000, offset=OFFSET)
    client.run()
    start_time = time.time()
    for event_data in receiver.receive(timeout=100):
        jsonstring = event_data.body_as_json()
        #data = json.loads(jsonstring)
        #print(data)
        devicename = jsonstring['device']
        reading = jsonstring['reading']
        last_offset = event_data.offset
        last_sn = event_data.sequence_number
        print("Received: {}, {}".format(last_offset, last_sn))
        print(f"Device: {devicename} | Reading: {reading}")
        total += 1

    end_time = time.time()
    client.stop()
    run_time = end_time - start_time
    print("Received {} messages in {} seconds".format(total, run_time))

except KeyboardInterrupt:
    pass
finally:
    client.stop()